package org.discordlist.gateway.core;

import org.discordlist.gateway.io.rabbitmq.RabbitMQ;

public class RabbitMQUtil {

    private static Gateway gateway;

    public static void setGateway(Gateway gw) {
        gateway = gw;
    }

    public static String getQueue(RabbitMQ.Queues queue) {
        return gateway.getDev() ? "dev." + queue.getKey() : queue.getKey();
    }

    public static String getExchange(RabbitMQ.Exchanges exchange) {
        return gateway.getDev() ? "dev." + exchange.getKey() : exchange.getKey();
    }
}
