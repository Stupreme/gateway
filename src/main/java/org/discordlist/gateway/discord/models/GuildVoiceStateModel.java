package org.discordlist.gateway.discord.models;

import net.dv8tion.jda.core.entities.GuildVoiceState;
import org.jetbrains.annotations.Nullable;

public class GuildVoiceStateModel extends VoiceStateModel {

    private final Long guildId;
    private final Long userId;
    private final GuildMemberModel member;
    private final Boolean deaf;
    private final Boolean mute;
    private final Boolean suppress;

    public GuildVoiceStateModel(long channelId, @Nullable String sessionId, boolean selfDeaf, boolean selfMute, Long guildId, Long userId, GuildMemberModel member, Boolean deaf, Boolean mute, Boolean suppress) {
        super(channelId, sessionId, selfDeaf, selfMute);
        this.guildId = guildId;
        this.userId = userId;
        this.member = member;
        this.deaf = deaf;
        this.mute = mute;
        this.suppress = suppress;
    }

    public Long getGuildId() {
        return guildId;
    }

    public Long getUserId() {
        return userId;
    }

    public GuildMemberModel getMember() {
        return member;
    }

    public Boolean getDeaf() {
        return deaf;
    }

    public Boolean getMute() {
        return mute;
    }

    public Boolean getSuppress() {
        return suppress;
    }

    public static GuildVoiceStateModel fromGuildVoiceState(GuildVoiceState guildVoiceState) {
        return new GuildVoiceStateModel(guildVoiceState.getAudioChannel() == null ? 0L : guildVoiceState.getAudioChannel().getIdLong(), guildVoiceState.getSessionId(), guildVoiceState.isSelfDeafened(), guildVoiceState.isSelfMuted(), guildVoiceState.getGuild().getIdLong(), guildVoiceState.getMember().getUser().getIdLong(), GuildMemberModel.Companion.fromMember(guildVoiceState.getMember()), guildVoiceState.isDeafened(), guildVoiceState.isMuted(), guildVoiceState.isSuppressed());
    }
}
