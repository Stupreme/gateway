package org.discordlist.gateway.discord;

import net.dv8tion.jda.bot.sharding.DefaultShardManagerBuilder;
import net.dv8tion.jda.bot.sharding.ShardManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.discordlist.gateway.core.IGateway;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.simpleyaml.configuration.file.YamlFile;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.Objects;

public class Discord implements IDiscord {

    private final Logger log = LogManager.getLogger(getClass());
    private final IGateway gateway;
    private final YamlFile config;
    private ShardManager shardManager;

    public Discord(IGateway gateway, YamlFile config) {
        this.gateway = gateway;
        this.config = config;
    }

    public void launch(int[] shardIds, int totalShards) throws LoginException {
        log.info("[Discord] Launching shards with ids {} of {} shards...", shardIds, totalShards);
        shardManager = new DefaultShardManagerBuilder(config.getString("bot.token"))
                .setShards(shardIds)
                .setShardsTotal(totalShards)
                .build();
    }

    @NotNull
    @Override
    public ShardManager getShardManager() {
        return shardManager;
    }

    public static int fetchOptimalShardcount(String token) throws IOException {
        var request = new Request.Builder()
                .url("https://discordapp.com/api/gateway/bot")
                .addHeader("Authorization", token)
                .build();
        var res = new OkHttpClient().newCall(request).execute();
        if (res.code() != 200)
            throw new IOException(String.format("Got unexpected response code: %d", res.code()));
        var json = new JSONObject(Objects.requireNonNull(res.body()).string());
        if (json.has("shards"))
            return json.getInt("shards");
        throw new IOException("Response does not contain shard amount.");
    }
}
