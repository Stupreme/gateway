package org.discordlist.gateway

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.ConfigurationSource
import org.apache.logging.log4j.core.config.Configurator
import org.discordlist.gateway.core.Gateway

fun main(args: Array<String>) {
    Configurator.setRootLevel(if (args.isEmpty()) Level.INFO else Level.toLevel(args[0]))
    Configurator.initialize(
        ClassLoader.getSystemClassLoader(),
        ConfigurationSource(ClassLoader.getSystemResourceAsStream("log4j2.xml"))
    )
    // Launch
    Gateway()
}