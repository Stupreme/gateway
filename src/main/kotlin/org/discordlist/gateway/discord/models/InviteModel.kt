package org.discordlist.gateway.discord.models

import net.dv8tion.jda.core.entities.Invite

data class InviteModel(
    val code: String,
    val guild: InviteGuildModel?,
    val channel: InviteChannelModel,
    val approximatePresenceCount: Int?,
    val approximateMemberCount: Int?
) {
    companion object {
        fun fromInvite(invite: Invite): InviteModel {
            return InviteModel(
                invite.code,
                InviteGuildModel.fromGuild(invite.guild),
                InviteChannelModel.fromChannel(invite.channel),
                invite.maxAge,
                invite.maxUses
            )
        }
    }
}

data class InviteChannelModel(
    val id: Long,
    val name: String,
    val type: Int
) {
    companion object {
        fun fromChannel(channel: Invite.Channel): InviteChannelModel {
            return InviteChannelModel(
                channel.idLong,
                channel.name,
                channel.type.id
            )

        }
    }
}

data class InviteGuildModel(
    val id: Long,
    val iconHash: String,
    val splashHash: String?,
    val verificationLevel: Int,
    val onlineCount: Int,
    val memberCount: Int,
    val features: Array<String>

) {

    companion object {
        fun fromGuild(guild: Invite.Guild): InviteGuildModel {
            return InviteGuildModel(
                guild.idLong,
                guild.iconId,
                guild.splashId,
                guild.verificationLevel.key,
                guild.onlineCount,
                guild.memberCount,
                guild.features.toTypedArray()
            )
        }

    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GuildModel

        if (id != other.id) return false
        if (iconHash != other.iconHash) return false
        if (splashHash != other.splashHash) return false
        if (verificationLevel != other.verificationLevel) return false
        if (memberCount != other.memberCount) return false
        if (!features.contentEquals(other.features)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + iconHash.hashCode()
        result = 31 * result + (splashHash?.hashCode() ?: 0)
        result = 31 * result + verificationLevel
        result = 31 * result + onlineCount
        result = 31 * result + memberCount
        result = 31 * result + features.contentHashCode()
        return result
    }
}