package org.discordlist.gateway.discord.models

import net.dv8tion.jda.core.entities.User

data class UserModel(
    val id: Long,
    val username: String,
    val discriminator: String,
    val avatarHash: String?,
    val bot: Boolean,
    val fake: Boolean,
    val creationTime: Long
) {

    companion object {

        fun fromUser(user: User): UserModel {
            return UserModel(
                user.idLong,
                user.name,
                user.discriminator,
                user.avatarId,
                user.isBot,
                user.isFake,
                user.creationTime.toEpochSecond()
            )
        }
    }
}