package org.discordlist.gateway.discord.models

import net.dv8tion.jda.core.entities.Member

data class GuildMemberModel(
    val user: UserModel,
    val nick: String?,
    val roles: Array<RoleModel>,
    val joinedAt: Long
) {

    companion object {
        fun fromMember(member: Member): GuildMemberModel {
            return GuildMemberModel(
                UserModel.fromUser(member.user),
                member.nickname,
                member.roles.map { RoleModel.fromRole(it) }.toTypedArray(),
                member.joinDate.toInstant().toEpochMilli()
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GuildMemberModel

        if (user != other.user) return false
        if (nick != other.nick) return false
        if (!roles.contentEquals(other.roles)) return false
        if (joinedAt != other.joinedAt) return false

        return true
    }

    override fun hashCode(): Int {
        var result = user.hashCode()
        result = 31 * result + (nick?.hashCode() ?: 0)
        result = 31 * result + roles.contentHashCode()
        result = 31 * result + joinedAt.hashCode()
        return result
    }

}