package org.discordlist.gateway.discord.models

import net.dv8tion.jda.core.entities.Emote

data class EmojiModel(
    val id: Long,
    val name: String,
    val roles: Array<RoleModel>?,
    val managed: Boolean?,
    val animated: Boolean?

) {

    companion object {
        fun fromEmote(emote: Emote): EmojiModel {
            return EmojiModel(
                emote.idLong,
                emote.name,
                emote.roles.map { RoleModel.fromRole(it) }.toTypedArray(),
                emote.isManaged,
                emote.isAnimated
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EmojiModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (roles != null) {
            if (other.roles == null) return false
            if (!roles.contentEquals(other.roles)) return false
        } else if (other.roles != null) return false
        if (managed != other.managed) return false
        if (animated != other.animated) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (roles?.contentHashCode() ?: 0)
        result = 31 * result + (managed?.hashCode() ?: 0)
        result = 31 * result + (animated?.hashCode() ?: 0)
        return result
    }


}