package org.discordlist.gateway.discord.models

import net.dv8tion.jda.core.entities.Webhook

data class WebhookModel(
    val id: Long,
    val guildId: Long?,
    val channelId: Long,
    val user: UserModel,
    val name: String?,
    val avatar: String?,
    val token: String?
) {
    companion object {
        fun fromWebhook(webhook: Webhook): WebhookModel {
            return WebhookModel(
                webhook.idLong,
                if (webhook.guild == null) null else webhook.guild.idLong,
                webhook.channel.idLong,
                UserModel.fromUser(webhook.defaultUser),
                webhook.defaultUser.name,
                webhook.defaultUser.avatarId,
                webhook.token
            )
        }
    }

}