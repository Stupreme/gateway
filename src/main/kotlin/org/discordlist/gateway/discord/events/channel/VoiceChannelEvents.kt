package org.discordlist.gateway.discord.events.channel

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.channel.voice.VoiceChannelCreateEvent
import net.dv8tion.jda.core.events.channel.voice.VoiceChannelDeleteEvent
import net.dv8tion.jda.core.events.channel.voice.update.*
import org.discordlist.gateway.discord.models.CategoryModel
import org.discordlist.gateway.discord.models.VoiceChannelModel

open class GenericGatewayVoiceChannelEvent(
    val channel: VoiceChannelModel
)

class GatewayVoiceChannelCreateEvent(channel: VoiceChannelModel) : GenericGatewayVoiceChannelEvent(channel) {
    companion object {
        fun fromVoiceChannelCreateEvent(voiceChannelCreateEvent: VoiceChannelCreateEvent): GatewayVoiceChannelCreateEvent {
            return GatewayVoiceChannelCreateEvent(
                VoiceChannelModel.fromVoiceChannel(voiceChannelCreateEvent.channel)
            )
        }
    }
}

class GatewayVoiceChannelDeleteEvent(channel: VoiceChannelModel) : GenericGatewayVoiceChannelEvent(channel) {
    companion object {
        fun fromVoiceChannelDeleteEvent(voiceChannelCreateEvent: VoiceChannelDeleteEvent): GatewayVoiceChannelDeleteEvent {
            return GatewayVoiceChannelDeleteEvent(
                VoiceChannelModel.fromVoiceChannel(voiceChannelCreateEvent.channel)
            )
        }
    }
}

abstract class GenericGatewayVoiceChannelUpdateEvent<T>(
    channel: VoiceChannelModel,
    private val identifier: String,
    private val previous: T,
    private val next: T
) :
    GenericGatewayVoiceChannelEvent(
        channel
    ), UpdateEvent<VoiceChannelModel, T> {
    override fun getEntity(): VoiceChannelModel {
        return channel
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }
}

class GatewayVoiceChannelUpdateUserLimitEvent(channel: VoiceChannelModel, previous: Int, next: Int) :
    GenericGatewayVoiceChannelUpdateEvent<Int>(channel, "userlimit", previous, next) {
    companion object {
        fun fromevent(event: VoiceChannelUpdateUserLimitEvent): GatewayVoiceChannelUpdateUserLimitEvent {
            return GatewayVoiceChannelUpdateUserLimitEvent(
                VoiceChannelModel.fromVoiceChannel(event.channel),
                event.oldUserLimit,
                event.newUserLimit
            )
        }
    }
}

class GatewayVoiceChannelUpdatePositionEvent(channel: VoiceChannelModel, previous: Int, next: Int) :
    GenericGatewayVoiceChannelUpdateEvent<Int>(channel, "position", previous, next) {
    companion object {
        fun fromVoiceChannelUpdatePositionEvent(event: VoiceChannelUpdatePositionEvent): GatewayVoiceChannelUpdatePositionEvent {
            return GatewayVoiceChannelUpdatePositionEvent(
                VoiceChannelModel.fromVoiceChannel(event.channel),
                event.oldPosition,
                event.newPosition
            )
        }
    }
}

class GatewayVoiceChannelUpdateParentEvent(
    channel: VoiceChannelModel,
    previous: CategoryModel,
    next: CategoryModel
) : GenericGatewayVoiceChannelUpdateEvent<CategoryModel>(channel, "parent", previous, next) {
    companion object {
        fun fromVoiceChannelUpdateParentEvent(voiceChannelUpdateParentEvent: VoiceChannelUpdateParentEvent): GatewayVoiceChannelUpdateParentEvent {
            return GatewayVoiceChannelUpdateParentEvent(
                VoiceChannelModel.fromVoiceChannel(voiceChannelUpdateParentEvent.channel),
                CategoryModel.fromCategory(voiceChannelUpdateParentEvent.oldParent),
                CategoryModel.fromCategory(voiceChannelUpdateParentEvent.newParent)
            )
        }
    }
}

class GatewayVoiceChannelUpdateNameEvent(channel: VoiceChannelModel, previous: String, next: String) :
    GenericGatewayVoiceChannelUpdateEvent<String>(channel, "name", previous, next) {
    companion object {
        fun fromVoiceChannelUpdateNameEvent(event: VoiceChannelUpdateNameEvent): GatewayVoiceChannelUpdateNameEvent {
            return GatewayVoiceChannelUpdateNameEvent(
                VoiceChannelModel.fromVoiceChannel(event.channel),
                event.oldName,
                event.newName
            )
        }
    }
}

class GatewayVoiceChannelUpdateBitrateEvent(channel: VoiceChannelModel, previous: Int, next: Int) :
    GenericGatewayVoiceChannelUpdateEvent<Int>(channel, "bitrate", previous, next) {
    companion object {
        fun fromVoiceChannelUpdateBitrateEvent(event: VoiceChannelUpdateBitrateEvent): GatewayVoiceChannelUpdateBitrateEvent {
            return GatewayVoiceChannelUpdateBitrateEvent(
                VoiceChannelModel.fromVoiceChannel(event.channel),
                event.oldBitrate,
                event.newBitrate
            )
        }
    }
}