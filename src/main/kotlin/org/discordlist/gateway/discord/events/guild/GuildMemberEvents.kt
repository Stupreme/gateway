package org.discordlist.gateway.discord.events.guild

import net.dv8tion.jda.core.events.guild.member.*
import org.discordlist.gateway.discord.models.GuildMemberModel
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.RoleModel

open class GenericGatewayGuildMemberEvent(
    guild: GuildModel,
    val member: GuildMemberModel
) :
    GenericGatewayGuildEvent(guild)

class GatewayGuildMemberJoinEvent(guild: GuildModel, member: GuildMemberModel) :
    GenericGatewayGuildMemberEvent(guild, member) {
    companion object {
        fun fromGuildMemberJoinEvent(event: GuildMemberJoinEvent): GatewayGuildMemberJoinEvent {
            return GatewayGuildMemberJoinEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member)
            )
        }
    }
}

class GatewayGuildMemberLeaveEvent(guild: GuildModel, member: GuildMemberModel) :
    GenericGatewayGuildMemberEvent(guild, member) {
    companion object {
        fun fromGuildMemberLeaveEvent(event: GuildMemberLeaveEvent): GatewayGuildMemberLeaveEvent {
            return GatewayGuildMemberLeaveEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member)
            )
        }
    }
}

class GatewayGuildMemberNickChangeEvent(
    val prevNick: String,
    val newNick: String, guild: GuildModel, member: GuildMemberModel
) : GenericGatewayGuildMemberEvent(guild, member) {
    companion object {
        fun fromGuildMemberNickChangeEvent(event: GuildMemberNickChangeEvent): GatewayGuildMemberNickChangeEvent {
            return GatewayGuildMemberNickChangeEvent(
                event.prevNick,
                event.newNick,
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member)
            )
        }
    }
}

class GatewayGuildMemberRoleAddEvent(
    guild: GuildModel, member: GuildMemberModel,
    val addedRoles: List<RoleModel>
) :
    GenericGatewayGuildMemberEvent(guild, member) {
    companion object {
        fun fromGuildMemberRoleAddEvent(event: GuildMemberRoleAddEvent): GatewayGuildMemberRoleAddEvent {
            return GatewayGuildMemberRoleAddEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.roles.map { RoleModel.fromRole(it) }
            )
        }
    }
}

class GatewayGuildMemberRoleRemoveEvent(
    guild: GuildModel, member: GuildMemberModel,
    val RemoveedRoles: List<RoleModel>
) :
    GenericGatewayGuildMemberEvent(guild, member) {
    companion object {
        fun fromGuildMemberRoleRemoveEvent(event: GuildMemberRoleRemoveEvent): GatewayGuildMemberRoleRemoveEvent {
            return GatewayGuildMemberRoleRemoveEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.roles.map { RoleModel.fromRole(it) }
            )
        }
    }
}
