package org.discordlist.gateway.discord.events.message

import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveAllEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent
import org.discordlist.gateway.discord.models.MessageChannelModel
import org.discordlist.gateway.discord.models.ReactionModel
import org.discordlist.gateway.discord.models.UserModel

open class GenericGatewayMessageReactionEvent(
    messageId: Long,
    channel: MessageChannelModel,
    val issuer: UserModel,
    val reaction: ReactionModel
) : GenericGatewayMessageEvent(messageId, channel)

class GatewayMessageReactionAddEvent(
    messageId: Long,
    channel: MessageChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayMessageReactionEvent(messageId, channel, issuer, reaction) {
    companion object {
        fun fromMessageReactionAddEvent(event: MessageReactionAddEvent): GatewayMessageReactionAddEvent {
            return GatewayMessageReactionAddEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}

class GatewayMessageReactionRemoveEvent(
    messageId: Long,
    channel: MessageChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayMessageReactionEvent(messageId, channel, issuer, reaction) {
    companion object {
        fun fromMessageReactionRemoveEvent(event: MessageReactionRemoveEvent): GatewayMessageReactionRemoveEvent {
            return GatewayMessageReactionRemoveEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}

class GatewayMessageReactionRemoveAllEvent(messageId: Long, channel: MessageChannelModel) :
    GenericGatewayMessageEvent(messageId, channel) {
    companion object {
        fun fromMessageReactionRemoveAllEvent(event: MessageReactionRemoveAllEvent): GatewayMessageReactionRemoveAllEvent {
            return GatewayMessageReactionRemoveAllEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel)
            )
        }
    }
}

