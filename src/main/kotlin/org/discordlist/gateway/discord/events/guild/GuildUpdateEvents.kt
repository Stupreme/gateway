package org.discordlist.gateway.discord.events.guild

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.guild.update.*
import org.discordlist.gateway.discord.models.GuildMemberModel
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.TextChannelModel

open class GenericGatewayGuildUpdateEvent<T>(
    guild: GuildModel,
    private val previous: T,
    private val next: T,
    private val identifier: String
) : GenericGatewayGuildEvent(guild), UpdateEvent<GuildModel, T> {
    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getEntity(): GuildModel {
        return guild
    }
}

class GatewayGuildUpdateAfkTimeoutEvent(
    guild: GuildModel,
    previous: Int,
    next: Int
) : GenericGatewayGuildUpdateEvent<Int>(guild, previous, next, "afk_timeout") {
    companion object {
        fun fromGuildUpdateAfkTimeoutEvent(event: GuildUpdateAfkTimeoutEvent): GatewayGuildUpdateAfkTimeoutEvent {
            return GatewayGuildUpdateAfkTimeoutEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.seconds,
                event.newValue.seconds
            )
        }
    }
}

class GatewayGuildUpdateExplicitContentLevelEvent(
    guild: GuildModel,
    previous: Int,
    next: Int
) : GenericGatewayGuildUpdateEvent<Int>(guild, previous, next, "explicit_content_filter") {
    companion object {
        fun fromGuildUpdateExplicitContentLevelEvent(event: GuildUpdateExplicitContentLevelEvent): GatewayGuildUpdateExplicitContentLevelEvent {
            return GatewayGuildUpdateExplicitContentLevelEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.key,
                event.newValue.key
            )
        }
    }
}

class GatewayGuildUpdateFeaturesEvent(
    guild: GuildModel,
    previous: Array<String>,
    next: Array<String>
) : GenericGatewayGuildUpdateEvent<Array<String>>(guild, previous, next, "features") {
    companion object {
        fun fromGuildUpdateFeaturesEvent(event: GuildUpdateFeaturesEvent): GatewayGuildUpdateFeaturesEvent {
            return GatewayGuildUpdateFeaturesEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.toTypedArray(),
                event.newValue.toTypedArray()
            )
        }
    }
}

class GatewayGuildUpdateMFALevelEvent(
    guild: GuildModel,
    previous: Int,
    next: Int
) : GenericGatewayGuildUpdateEvent<Int>(guild, previous, next, "afk_channel") {
    companion object {
        fun fromGuildUpdateMFALevelEvent(event: GuildUpdateMFALevelEvent): GatewayGuildUpdateMFALevelEvent {
            return GatewayGuildUpdateMFALevelEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.key,
                event.newValue.key
            )
        }
    }
}

class GatewayGuildUpdateIconEvent(
    guild: GuildModel,
    previous: String,
    next: String
) : GenericGatewayGuildUpdateEvent<String>(guild, previous, next, "icon") {
    companion object {
        fun fromGuildUpdateIconEvent(event: GuildUpdateIconEvent): GatewayGuildUpdateIconEvent {
            return GatewayGuildUpdateIconEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayGuildUpdateNotificationLevelEvent(
    guild: GuildModel,
    previous: Int,
    next: Int
) : GenericGatewayGuildUpdateEvent<Int>(guild, previous, next, "notification_level") {
    companion object {
        fun fromGuildUpdateNotificationLevelEvent(event: GuildUpdateNotificationLevelEvent): GatewayGuildUpdateNotificationLevelEvent {
            return GatewayGuildUpdateNotificationLevelEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.key,
                event.newValue.key
            )
        }
    }
}

class GatewayGuildUpdateOwnerEvent(
    guild: GuildModel,
    previous: GuildMemberModel,
    next: GuildMemberModel
) : GenericGatewayGuildUpdateEvent<GuildMemberModel>(guild, previous, next, "owner") {
    companion object {
        fun fromGuildUpdateOwnerEvent(event: GuildUpdateOwnerEvent): GatewayGuildUpdateOwnerEvent {
            return GatewayGuildUpdateOwnerEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.oldValue),
                GuildMemberModel.fromMember(event.newOwner)
            )
        }
    }
}

class GatewayGuildUpdateRegionEvent(
    guild: GuildModel,
    previous: String,
    next: String
) : GenericGatewayGuildUpdateEvent<String>(guild, previous, next, "notification_level") {
    companion object {
        fun fromGuildUpdateRegionEvent(event: GuildUpdateRegionEvent): GatewayGuildUpdateRegionEvent {
            return GatewayGuildUpdateRegionEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.key,
                event.newValue.key
            )
        }
    }
}

class GatewayGuildUpdateSplashEvent(
    guild: GuildModel,
    previous: String,
    next: String
) : GenericGatewayGuildUpdateEvent<String>(guild, previous, next, "notification_level") {
    companion object {
        fun fromGuildUpdateSplashEvent(event: GuildUpdateSplashEvent): GatewayGuildUpdateSplashEvent {
            return GatewayGuildUpdateSplashEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayGuildUpdateSystemChannelEvent(
    guild: GuildModel,
    previous: TextChannelModel,
    next: TextChannelModel
) : GenericGatewayGuildUpdateEvent<TextChannelModel>(guild, previous, next, "notification_level") {
    companion object {
        fun fromGuildUpdateSystemChannelEvent(event: GuildUpdateSystemChannelEvent): GatewayGuildUpdateSystemChannelEvent {
            return GatewayGuildUpdateSystemChannelEvent(
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.oldValue),
                TextChannelModel.fromTextChannel(event.newValue)
            )
        }
    }
}

class GatewayGuildUpdateVerificationLevelEvent(
    guild: GuildModel,
    previous: Int,
    next: Int
) : GenericGatewayGuildUpdateEvent<Int>(guild, previous, next, "Verification_level") {
    companion object {
        fun fromGuildUpdateVerificationLevelEvent(event: GuildUpdateVerificationLevelEvent): GatewayGuildUpdateVerificationLevelEvent {
            return GatewayGuildUpdateVerificationLevelEvent(
                GuildModel.fromGuild(event.guild),
                event.oldValue.key,
                event.newValue.key
            )
        }
    }
}