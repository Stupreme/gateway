package org.discordlist.gateway.discord.events.emote

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.emote.EmoteAddedEvent
import net.dv8tion.jda.core.events.emote.EmoteRemovedEvent
import net.dv8tion.jda.core.events.emote.update.EmoteUpdateNameEvent
import net.dv8tion.jda.core.events.emote.update.EmoteUpdateRolesEvent
import org.discordlist.gateway.discord.models.EmojiModel
import org.discordlist.gateway.discord.models.RoleModel

open class GenericGatewayEmojiEvent(
    val emoji: EmojiModel
)

class GatewayEmojiRemovedEvent(emoji: EmojiModel) : GenericGatewayEmojiEvent(emoji) {
    companion object {
        fun fromEmoteRemovedEvent(event: EmoteRemovedEvent): GatewayEmojiRemovedEvent {
            return GatewayEmojiRemovedEvent(
                EmojiModel.fromEmote(event.emote)
            )
        }
    }
}

class GatewayEmojiAddedEvent(emoji: EmojiModel) : GenericGatewayEmojiEvent(emoji) {
    companion object {
        fun fromEmoteAddedEvent(event: EmoteAddedEvent): GatewayEmojiAddedEvent {
            return GatewayEmojiAddedEvent(
                EmojiModel.fromEmote(event.emote)
            )
        }
    }
}

open class GenericGatwayEmojiUpdateEvent<T>(
    emoji: EmojiModel,
    val previous: T,
    val next: T,
    val identifier: String
) : GenericGatewayEmojiEvent(emoji), UpdateEvent<EmojiModel, T> {
    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getEntity(): EmojiModel {
        return emoji
    }

}

class GatewayEmojiUpdateNameEvent(
    emoji: EmojiModel,
    previous: String,
    next: String
) :
    GenericGatwayEmojiUpdateEvent<String>(emoji, previous, next, "name") {

    companion object {
        fun fromEmoteUpdateNameEvent(event: EmoteUpdateNameEvent): GatewayEmojiUpdateNameEvent {
            return GatewayEmojiUpdateNameEvent(
                EmojiModel.fromEmote(event.emote),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayEmojiUpdateRolesEvent(
    emoji: EmojiModel,
    previous: Array<RoleModel>,
    next: Array<RoleModel>
) :
    GenericGatwayEmojiUpdateEvent<Array<RoleModel>>(emoji, previous, next, "Roles") {

    companion object {
        fun fromEmoteUpdateRolesEvent(event: EmoteUpdateRolesEvent): GatewayEmojiUpdateRolesEvent {
            return GatewayEmojiUpdateRolesEvent(
                EmojiModel.fromEmote(event.emote),
                event.oldValue.map { RoleModel.fromRole(it) }.toTypedArray(),
                event.newValue.map { RoleModel.fromRole(it) }.toTypedArray()
            )
        }
    }
}