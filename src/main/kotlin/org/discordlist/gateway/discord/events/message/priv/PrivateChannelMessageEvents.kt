package org.discordlist.gateway.discord.events.message.priv

import net.dv8tion.jda.core.events.message.priv.PrivateMessageDeleteEvent
import net.dv8tion.jda.core.events.message.priv.PrivateMessageEmbedEvent
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.core.events.message.priv.PrivateMessageUpdateEvent
import org.discordlist.gateway.discord.models.MessageModel
import org.discordlist.gateway.discord.models.PrivateChannelModel

open class GenericGatewayPrivateMessageEvent(
    val messageId: Long,
    val channel: PrivateChannelModel
)

class GatewayPrivateMessageDeleteEvent(messageId: Long, channel: PrivateChannelModel) :
    GenericGatewayPrivateMessageEvent(messageId, channel) {
    companion object {
        fun fromPrivateMessageDeleteEvent(event: PrivateMessageDeleteEvent): GatewayPrivateMessageDeleteEvent {
            return GatewayPrivateMessageDeleteEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel)
            )
        }
    }
}

class GatewayPrivateMessageEmbedEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    val embeds: Array<String>
) :
    GenericGatewayPrivateMessageEvent(messageId, channel) {
    companion object {
        fun fromPrivateMessageEmbedEvent(event: PrivateMessageEmbedEvent): GatewayPrivateMessageEmbedEvent {
            return GatewayPrivateMessageEmbedEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel),
                event.messageEmbeds.map { it.toJSONObject().toString() }.toTypedArray()
            )
        }
    }
}

class GatewayPrivateMessageReceivedEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    val message: MessageModel
) :
    GenericGatewayPrivateMessageEvent(messageId, channel) {
    companion object {
        fun fromPrivateMessageReceivedEvent(event: PrivateMessageReceivedEvent): GatewayPrivateMessageReceivedEvent {
            return GatewayPrivateMessageReceivedEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}

class GatewayPrivateMessageUpdateEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    val message: MessageModel
) :
    GenericGatewayPrivateMessageEvent(messageId, channel) {
    companion object {
        fun fromPrivateMessageUpdateEvent(event: PrivateMessageUpdateEvent): GatewayPrivateMessageUpdateEvent {
            return GatewayPrivateMessageUpdateEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}