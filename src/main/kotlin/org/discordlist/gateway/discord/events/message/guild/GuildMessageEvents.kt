package org.discordlist.gateway.discord.events.message.guild

import net.dv8tion.jda.core.events.message.MessageEmbedEvent
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent
import org.discordlist.gateway.discord.events.guild.GenericGatewayGuildEvent
import org.discordlist.gateway.discord.events.message.GatewayMessageEmbedEvent
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.MessageChannelModel
import org.discordlist.gateway.discord.models.MessageModel
import org.discordlist.gateway.discord.models.TextChannelModel

open class GenericGatewayGuildMessageEvent(
    val messageId: Long,
    guild: GuildModel,
    val textChannel: TextChannelModel
) :
    GenericGatewayGuildEvent(guild)

class GatewayGuildMessageDeleteEvent(messageId: Long, guild: GuildModel, textChannel: TextChannelModel) :
    GenericGatewayGuildMessageEvent(messageId, guild, textChannel) {
    companion object {
        fun fromGuildMessageDeleteEvent(event: GuildMessageDeleteEvent): GatewayGuildMessageDeleteEvent {
            return GatewayGuildMessageDeleteEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel)
            )
        }
    }
}


class GatewayGuildMessageEmbedEvent(
    messageId: Long,
    guild: GuildModel,
    channel: TextChannelModel,
    val embed: Array<String>
) :
    GenericGatewayGuildMessageEvent(messageId, guild, channel) {
    companion object {
        fun fromMessageEmbedEvent(event: MessageEmbedEvent): GatewayMessageEmbedEvent {
            return GatewayMessageEmbedEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                event.messageEmbeds.map { it.toJSONObject().toString(2) }.toTypedArray()
            )
        }
    }
}

class GatewayGuildMessageReceivedEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel,
    val message: MessageModel
) :
    GenericGatewayGuildMessageEvent(messageId, guild, textChannel) {
    companion object {
        fun fromGuildMessageReceivedEvent(event: GuildMessageReceivedEvent): GatewayGuildMessageReceivedEvent {
            return GatewayGuildMessageReceivedEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}

class GatewayGuildMessageUpdateEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel,
    val message: MessageModel
) :
    GenericGatewayGuildMessageEvent(messageId, guild, textChannel) {
    companion object {
        fun fromGuildMessageUpdateEvent(event: GuildMessageUpdateEvent): GatewayGuildMessageUpdateEvent {
            return GatewayGuildMessageUpdateEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}