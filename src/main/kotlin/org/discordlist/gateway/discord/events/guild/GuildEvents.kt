package org.discordlist.gateway.discord.events.guild

import net.dv8tion.jda.core.events.guild.*
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.UserModel

open class GenericGatewayGuildEvent(
    val guild: GuildModel
)

class GatewayGuildAvailableEvent(guild: GuildModel) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildAvailableEvent(event: GuildAvailableEvent): GatewayGuildAvailableEvent {
            return GatewayGuildAvailableEvent(GuildModel.fromGuild(event.guild))
        }
    }
}

class GatewayGuildBanEvent(
    guild: GuildModel,
    val user: UserModel
) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildBanEvent(event: GuildBanEvent): GatewayGuildBanEvent {
            return GatewayGuildBanEvent(
                GuildModel.fromGuild(event.guild),
                UserModel.fromUser(event.user)
                )
        }
    }
}

class GatewayGuildUnbanEvent(
    guild: GuildModel,
    val user: UserModel
) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildUnbanEvent(event: GuildUnbanEvent): GatewayGuildUnbanEvent {
            return GatewayGuildUnbanEvent(
                GuildModel.fromGuild(event.guild),
                UserModel.fromUser(event.user)
            )
        }
    }
}

class GatewayGuildJoinEvent(guild: GuildModel) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildJoinEvent(event: GuildJoinEvent): GatewayGuildJoinEvent {
            return GatewayGuildJoinEvent(GuildModel.fromGuild(event.guild))
        }
    }
}


class GatewayGuildLeaveEvent(guild: GuildModel) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildLeaveEvent(event: GuildLeaveEvent): GatewayGuildLeaveEvent {
            return GatewayGuildLeaveEvent(GuildModel.fromGuild(event.guild))
        }
    }
}

class GatewayGuildReadyEvent(guild: GuildModel) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildReadyEvent(event: GuildReadyEvent): GatewayGuildReadyEvent {
            return GatewayGuildReadyEvent(GuildModel.fromGuild(event.guild))
        }
    }
}

class GatewayGuildUnavailableEvent(guild: GuildModel) : GenericGatewayGuildEvent(guild) {
    companion object {
        fun fromGuildUnavailableEvent(event: GuildUnavailableEvent): GatewayGuildUnavailableEvent {
            return GatewayGuildUnavailableEvent(GuildModel.fromGuild(event.guild))
        }
    }
}

class GatewayUnavailableGuildJoinedEvent(
    val guildId: Long
) {
    companion object {
        fun fromUnavailableGuildJoinEvent(event: UnavailableGuildJoinedEvent): GatewayUnavailableGuildJoinedEvent {
            return GatewayUnavailableGuildJoinedEvent(event.guildIdLong)
        }
    }
}
