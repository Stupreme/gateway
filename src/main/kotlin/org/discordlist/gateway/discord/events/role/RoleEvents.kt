package org.discordlist.gateway.discord.events.role

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.role.RoleCreateEvent
import net.dv8tion.jda.core.events.role.RoleDeleteEvent
import net.dv8tion.jda.core.events.role.update.*
import org.discordlist.gateway.discord.models.RoleModel

open class GenericGatewayRoleEvent(
    val role: RoleModel
)

class GatewayRoleCreateEvent(role: RoleModel) : GenericGatewayRoleEvent(role) {
    companion object {
        fun fromRoleCreateEvent(event: RoleCreateEvent): GatewayRoleCreateEvent {
            return GatewayRoleCreateEvent(
                RoleModel.fromRole(event.role)
            )
        }
    }
}

class GatewayRoleDeleteEvent(role: RoleModel) : GenericGatewayRoleEvent(role) {
    companion object {
        fun fromRoleDeleteEvent(event: RoleDeleteEvent): GatewayRoleDeleteEvent {
            return GatewayRoleDeleteEvent(
                RoleModel.fromRole(event.role)
            )
        }
    }
}

open class GenericGatewayRoleUpdateEvent<T>(
    role: RoleModel,
    private val previous: T,
    val next: T,
    private val identifier: String
) : GenericGatewayRoleEvent(role), UpdateEvent<RoleModel, T> {
    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getEntity(): RoleModel {
        return role
    }
}

class GatewayRoleUpdateColorEvent(
    role: RoleModel,
    previous: Int,
    next: Int
) :
    GenericGatewayRoleUpdateEvent<Int>(role, previous, next, "color") {
    companion object {
        fun fromRoleUpdateColorEvent(event: RoleUpdateColorEvent): GatewayRoleUpdateColorEvent {
            return GatewayRoleUpdateColorEvent(
                RoleModel.fromRole(event.role),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayRoleUpdateHoistedEvent(
    role: RoleModel,
    previous: Boolean,
    next: Boolean
) :
    GenericGatewayRoleUpdateEvent<Boolean>(role, previous, next, "Hoisted") {
    companion object {
        fun fromRoleUpdateHoistedEvent(event: RoleUpdateHoistedEvent): GatewayRoleUpdateHoistedEvent {
            return GatewayRoleUpdateHoistedEvent(
                RoleModel.fromRole(event.role),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayRoleUpdateMentionableEvent(
    role: RoleModel,
    previous: Boolean,
    next: Boolean
) :
    GenericGatewayRoleUpdateEvent<Boolean>(role, previous, next, "Mentionable") {
    companion object {
        fun fromRoleUpdateMentionableEvent(event: RoleUpdateMentionableEvent): GatewayRoleUpdateMentionableEvent {
            return GatewayRoleUpdateMentionableEvent(
                RoleModel.fromRole(event.role),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayRoleUpdateNameEvent(
    role: RoleModel,
    previous: String,
    next: String
) :
    GenericGatewayRoleUpdateEvent<String>(role, previous, next, "Name") {
    companion object {
        fun fromRoleUpdateNameEvent(event: RoleUpdateNameEvent): GatewayRoleUpdateNameEvent {
            return GatewayRoleUpdateNameEvent(
                RoleModel.fromRole(event.role),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayRoleUpdatePositionEvent(
    role: RoleModel,
    previous: Int,
    next: Int
) :
    GenericGatewayRoleUpdateEvent<Int>(role, previous, next, "Position") {
    companion object {
        fun fromRoleUpdatePositionEvent(event: RoleUpdatePositionEvent): GatewayRoleUpdatePositionEvent {
            return GatewayRoleUpdatePositionEvent(
                RoleModel.fromRole(event.role),
                event.oldValue,
                event.newValue
            )
        }
    }
}
