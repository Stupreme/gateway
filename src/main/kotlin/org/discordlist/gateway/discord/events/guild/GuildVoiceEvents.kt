package org.discordlist.gateway.discord.events.guild

import net.dv8tion.jda.core.events.guild.voice.GuildVoiceDeafenEvent
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceLeaveEvent
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceMoveEvent
import org.discordlist.gateway.discord.models.GuildMemberModel
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.VoiceChannelModel

open class GenericGatewayGuildVoiceEvent(
    guild: GuildModel,
    val member: GuildMemberModel
) : GenericGatewayGuildEvent(guild)

class GatewayGuildVoiceDeafenEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    deafen: Boolean
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceDeafenEvent(event: GuildVoiceDeafenEvent): GatewayGuildVoiceDeafenEvent {
            return GatewayGuildVoiceDeafenEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.isDeafened
            )
        }
    }
}

class GatewayGuildVoiceGuildDeafenEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    guildDeafened: Boolean
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceGuildDeafenEvent(event: GuildVoiceDeafenEvent): GatewayGuildVoiceGuildDeafenEvent {
            return GatewayGuildVoiceGuildDeafenEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.isDeafened
            )
        }
    }
}

class GatewayGuildVoiceGuildMuteEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    guildMuted: Boolean
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceGuildMuteEvent(event: GuildVoiceDeafenEvent): GatewayGuildVoiceGuildMuteEvent {
            return GatewayGuildVoiceGuildMuteEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.isDeafened
            )
        }
    }
}

class GatewayGuildVoiceJoinEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    channelJoined: VoiceChannelModel
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceJoinEvent(event: GuildVoiceJoinEvent): GatewayGuildVoiceJoinEvent {
            return GatewayGuildVoiceJoinEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                VoiceChannelModel.fromVoiceChannel(event.channelJoined)
            )
        }
    }
}


class GatewayGuildVoiceLeaveEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    channelLeft: VoiceChannelModel
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceLeaveEvent(event: GuildVoiceLeaveEvent): GatewayGuildVoiceLeaveEvent {
            return GatewayGuildVoiceLeaveEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                VoiceChannelModel.fromVoiceChannel(event.channelLeft)
            )
        }
    }
}

class GatewayGuildVoiceMoveEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    channelSwitched: VoiceChannelModel
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceMoveEvent(event: GuildVoiceMoveEvent): GatewayGuildVoiceMoveEvent {
            return GatewayGuildVoiceMoveEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                VoiceChannelModel.fromVoiceChannel(event.channelLeft)
            )
        }
    }
}

class GatewayGuildVoiceMuteEvent(
    guild: GuildModel,
    member: GuildMemberModel,
    muted: Boolean
) :
    GenericGatewayGuildVoiceEvent(guild, member) {
    companion object {
        fun fromGuildVoiceMuteEvent(event: GuildVoiceDeafenEvent): GatewayGuildVoiceMuteEvent {
            return GatewayGuildVoiceMuteEvent(
                GuildModel.fromGuild(event.guild),
                GuildMemberModel.fromMember(event.member),
                event.isDeafened
            )
        }
    }
}