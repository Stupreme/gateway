package org.discordlist.gateway.discord.events.channel

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.channel.category.CategoryDeleteEvent
import net.dv8tion.jda.core.events.channel.category.update.CategoryUpdateNameEvent
import net.dv8tion.jda.core.events.channel.category.update.CategoryUpdatePositionEvent
import org.discordlist.gateway.discord.models.CategoryModel


// Category
open class GenericGatewayCategoryEvent(
    val category: CategoryModel
)

class GatewayCategoryDeleteEvent(category: CategoryModel) : GenericGatewayCategoryEvent(category) {
    companion object {
        fun fromCategoryDeleteEvent(categoryDeleteEvent: CategoryDeleteEvent): GatewayCategoryDeleteEvent {
            return GatewayCategoryDeleteEvent(
                CategoryModel.fromCategory(categoryDeleteEvent.category)
            )
        }
    }
}

class GatewayCategoryCreateEvent(category: CategoryModel) : GenericGatewayCategoryEvent(category) {
    companion object {
        fun fromCategoryCreateEvent(categoryDeleteEvent: CategoryDeleteEvent): GatewayCategoryCreateEvent {
            return GatewayCategoryCreateEvent(
                CategoryModel.fromCategory(categoryDeleteEvent.category)
            )
        }
    }
}

// Category update
open class GenericGatewayCategoryUpdateEvent<T>(
    category: CategoryModel,
    private val previous: T,
    val next: T,
    private val identifier: String
) : GenericGatewayCategoryEvent(category), UpdateEvent<CategoryModel, T> {
    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getEntity(): CategoryModel {
        return category
    }
}

class GatewayCategoryUpdatePositionEvent(category: CategoryModel, previous: Int, next: Int) :
    GenericGatewayCategoryUpdateEvent<Int>(category, previous, next, "POSITION") {
    companion object {
        fun fromCategoryUpdatePositionEvent(categoryUpdatePositionEvent: CategoryUpdatePositionEvent): GatewayCategoryUpdatePositionEvent {
            return GatewayCategoryUpdatePositionEvent(
                CategoryModel.fromCategory(
                    categoryUpdatePositionEvent.category
                ),
                categoryUpdatePositionEvent.oldPosition,
                categoryUpdatePositionEvent.newPosition

            )
        }
    }
}

class GatewayCategoryUpdateNameEvent(category: CategoryModel, previous: String, next: String):
    GenericGatewayCategoryUpdateEvent<String>(category, previous, next, "name") {
    companion object {
        fun fromCategoryUpdateNameEvent(categoryUpdateNameEvent: CategoryUpdateNameEvent): GatewayCategoryUpdateNameEvent {
            return GatewayCategoryUpdateNameEvent(
                CategoryModel.fromCategory(
                    categoryUpdateNameEvent.category
                ),
                categoryUpdateNameEvent.oldName,
                categoryUpdateNameEvent.newName
            )
        }
    }
}

