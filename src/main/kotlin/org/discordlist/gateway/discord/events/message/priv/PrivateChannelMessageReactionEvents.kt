package org.discordlist.gateway.discord.events.message.priv

import net.dv8tion.jda.core.events.message.priv.react.PrivateMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.priv.react.PrivateMessageReactionRemoveEvent
import org.discordlist.gateway.discord.models.PrivateChannelModel
import org.discordlist.gateway.discord.models.ReactionModel
import org.discordlist.gateway.discord.models.UserModel

open class GenericGatewayPrivateMessageReactionEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    val issuer: UserModel,
    val reaction: ReactionModel
) : GenericGatewayPrivateMessageEvent(messageId, channel)

class GatewayPrivateMessageReactionAddEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayPrivateMessageReactionEvent(messageId, channel, issuer, reaction) {
    companion object {
        fun fromPrivateMessageReactionAddEvent(event: PrivateMessageReactionAddEvent): GatewayPrivateMessageReactionAddEvent {
            return GatewayPrivateMessageReactionAddEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}

class GatewayPrivateMessageReactionRemoveEvent(
    messageId: Long,
    channel: PrivateChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayPrivateMessageReactionEvent(messageId, channel, issuer, reaction) {
    companion object {
        fun fromPrivateMessageReactionRemoveEvent(event: PrivateMessageReactionRemoveEvent): GatewayPrivateMessageReactionRemoveEvent {
            return GatewayPrivateMessageReactionRemoveEvent(
                event.messageIdLong,
                PrivateChannelModel.fromPrivateChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}