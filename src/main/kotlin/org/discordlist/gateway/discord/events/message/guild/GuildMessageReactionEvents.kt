package org.discordlist.gateway.discord.events.message.guild

import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveAllEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.ReactionModel
import org.discordlist.gateway.discord.models.TextChannelModel
import org.discordlist.gateway.discord.models.UserModel

open class GenericGatewayGuildMessageReactionEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel,
    val issuer: UserModel,
    val reaction: ReactionModel
) :
    GenericGatewayGuildMessageEvent(messageId, guild, textChannel)

class GatewayGuildMessageReactionAddEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayGuildMessageReactionEvent(messageId, guild, textChannel, issuer, reaction) {
    companion object {
        fun fromGuildMessageReactionAddEvent(event: GuildMessageReactionAddEvent): GatewayGuildMessageReactionAddEvent {
            return GatewayGuildMessageReactionAddEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}

class GatewayGuildMessageReactionRemoveEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel,
    issuer: UserModel,
    reaction: ReactionModel
) : GenericGatewayGuildMessageReactionEvent(messageId, guild, textChannel, issuer, reaction) {
    companion object {
        fun fromGuildMessageReactionRemoveEvent(event: GuildMessageReactionRemoveEvent): GatewayGuildMessageReactionRemoveEvent {
            return GatewayGuildMessageReactionRemoveEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel),
                UserModel.fromUser(event.user),
                ReactionModel.fromMessageReaction(event.reaction)
            )
        }
    }
}

class GatewayGuildMessageReactionRemoveAllEvent(
    messageId: Long,
    guild: GuildModel,
    textChannel: TextChannelModel
) : GenericGatewayGuildMessageEvent(messageId, guild, textChannel) {
    companion object {
        fun fromGuildMessageReactionRemoveAllEvent(event: GuildMessageReactionRemoveAllEvent): GatewayGuildMessageReactionRemoveAllEvent {
            return GatewayGuildMessageReactionRemoveAllEvent(
                event.messageIdLong,
                GuildModel.fromGuild(event.guild),
                TextChannelModel.fromTextChannel(event.channel)
            )
        }
    }
}