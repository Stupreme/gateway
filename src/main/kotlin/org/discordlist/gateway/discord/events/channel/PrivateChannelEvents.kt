package org.discordlist.gateway.discord.events.channel

import net.dv8tion.jda.core.events.channel.priv.PrivateChannelCreateEvent
import net.dv8tion.jda.core.events.channel.priv.PrivateChannelDeleteEvent
import org.discordlist.gateway.discord.models.PrivateChannelModel

class GatewayPrivateChannelCreateEvent(
    val channel: PrivateChannelModel
) {
    companion object {
        fun fromPrivateChannelCreateEvent(privateChannelCreateEvent: PrivateChannelCreateEvent): GatewayPrivateChannelCreateEvent {
            return GatewayPrivateChannelCreateEvent(
                PrivateChannelModel.fromPrivateChannel(
                    privateChannelCreateEvent.channel
                )
            )
        }
    }
}

class GatewayPrivateChannelDeleteEvent(
    val channel: PrivateChannelModel
) {
    companion object {
        fun fromPrivateChannelCreateEvent(privateChannelCreateEvent: PrivateChannelDeleteEvent): GatewayPrivateChannelDeleteEvent {
            return GatewayPrivateChannelDeleteEvent(
                PrivateChannelModel.fromPrivateChannel(
                    privateChannelCreateEvent.channel
                )
            )
        }
    }
}