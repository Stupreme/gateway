package org.discordlist.gateway.discord.events.channel

import net.dv8tion.jda.core.events.UpdateEvent
import net.dv8tion.jda.core.events.channel.text.TextChannelCreateEvent
import net.dv8tion.jda.core.events.channel.text.TextChannelDeleteEvent
import net.dv8tion.jda.core.events.channel.text.update.*
import org.discordlist.gateway.discord.models.CategoryModel
import org.discordlist.gateway.discord.models.TextChannelModel

open class GenericGatewayTextChannelEvent(
    val channel: TextChannelModel
)

class GatewayTextChannelCreateEvent(channel: TextChannelModel) : GenericGatewayTextChannelEvent(channel) {
    companion object {
        fun fromTextChannelCreateEvent(event: TextChannelCreateEvent): GatewayTextChannelCreateEvent {
            return GatewayTextChannelCreateEvent(
                TextChannelModel.fromTextChannel(event.channel)
            )
        }
    }
}


class GatewayTextChannelDeleteEvent(channel: TextChannelModel) : GenericGatewayTextChannelEvent(channel) {
    companion object {
        fun fromTextChannelDeleteEvent(event: TextChannelDeleteEvent): GatewayTextChannelDeleteEvent {
            return GatewayTextChannelDeleteEvent(
                TextChannelModel.fromTextChannel(event.channel)
            )
        }
    }
}

open class GenericGatewayTextChannelUpdateEvent<T>(
    channel: TextChannelModel,
    private val previous: T,
    private val next: T,
    private val identifier: String
) : GenericGatewayTextChannelEvent(channel), UpdateEvent<TextChannelModel, T> {
    override fun getOldValue(): T {
        return previous
    }

    override fun getNewValue(): T {
        return next
    }

    override fun getPropertyIdentifier(): String {
        return identifier
    }

    override fun getEntity(): TextChannelModel {
        return channel
    }
}

class GatewayTextChannelUpdateNSFWEvent(
    channel: TextChannelModel,
    previous: Boolean,
    next: Boolean
) : GenericGatewayTextChannelUpdateEvent<Boolean>(channel, previous, next, "nsfw") {
    companion object {
        fun fromTextChannelUpdateNSFWEvent(event: TextChannelUpdateNSFWEvent): GatewayTextChannelUpdateNSFWEvent {
            return GatewayTextChannelUpdateNSFWEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayTextChannelUpdateNameEvent(
    channel: TextChannelModel,
    previous: String,
    next: String
) : GenericGatewayTextChannelUpdateEvent<String>(channel, "name", previous, next) {
    companion object {
        fun GatewayTextChannelUpdateNameEvent(event: TextChannelUpdateNameEvent): GatewayTextChannelUpdateNameEvent {
            return GatewayTextChannelUpdateNameEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayTextChannelUpdateParentEvent(
    channel: TextChannelModel,
    previous: CategoryModel,
    next: CategoryModel
) : GenericGatewayTextChannelUpdateEvent<CategoryModel>(channel, previous, next, "parent") {
    companion object {
        fun GatewayTextChannelUpdateParentEvent(event: TextChannelUpdateParentEvent): GatewayTextChannelUpdateParentEvent {
            return GatewayTextChannelUpdateParentEvent(
                TextChannelModel.fromTextChannel(event.channel),
                CategoryModel.fromCategory(event.oldValue),
                CategoryModel.fromCategory(event.newValue)
            )
        }
    }
}

class GatewayTextChannelUpdatePositionEvent(
    channel: TextChannelModel,
    previous: Int,
    next: Int
) : GenericGatewayTextChannelUpdateEvent<Int>(channel, previous, next, "position") {
    companion object {
        fun GatewayTextChannelUpdatePositionEvent(event: TextChannelUpdatePositionEvent): GatewayTextChannelUpdatePositionEvent {
            return GatewayTextChannelUpdatePositionEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayTextChannelUpdateSlowmodeEvent(
    channel: TextChannelModel,
    previous: Int,
    next: Int
) : GenericGatewayTextChannelUpdateEvent<Int>(channel, previous, next, "slowmode") {
    companion object {
        fun GatewayTextChannelUpdateSlowmodeEvent(event: TextChannelUpdateSlowmodeEvent): GatewayTextChannelUpdateSlowmodeEvent {
            return GatewayTextChannelUpdateSlowmodeEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.oldValue,
                event.newValue
            )
        }
    }
}

class GatewayTextChannelUpdateTopicEvent(
    channel: TextChannelModel,
    previous: String,
    next: String
) : GenericGatewayTextChannelUpdateEvent<String>(channel, "topic", previous, next) {
    companion object {
        fun GatewayTextChannelUpdateTopicEvent(event: TextChannelUpdateTopicEvent): GatewayTextChannelUpdateTopicEvent {
            return GatewayTextChannelUpdateTopicEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.oldValue,
                event.newValue
            )
        }
    }
}