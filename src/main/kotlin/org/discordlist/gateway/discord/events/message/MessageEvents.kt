package org.discordlist.gateway.discord.events.message

import net.dv8tion.jda.core.events.message.*
import org.discordlist.gateway.discord.models.MessageChannelModel
import org.discordlist.gateway.discord.models.MessageModel
import org.discordlist.gateway.discord.models.TextChannelModel

open class GenericGatewayMessageEvent(
    val messageId: Long,
    val channel: MessageChannelModel
)

class GatewayMessageBulkDeleteEvent(
    val channel: TextChannelModel,
    val messageIds: Array<Long>
) {
    companion object {
        fun fromMessageBulkDeleteEvent(event: MessageBulkDeleteEvent): GatewayMessageBulkDeleteEvent {
            return GatewayMessageBulkDeleteEvent(
                TextChannelModel.fromTextChannel(event.channel),
                event.messageIds.map { it.toLong() }.toTypedArray()
            )
        }
    }
}

class GatewayMessageDeleteEvent(messageId: Long, channel: MessageChannelModel) :
    GenericGatewayMessageEvent(messageId, channel) {
    companion object {
        fun fromMessageDeleteEvent(event: MessageDeleteEvent): GatewayMessageDeleteEvent {
            return GatewayMessageDeleteEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel)
            )
        }
    }
}

class GatewayMessageEmbedEvent(
    messageId: Long,
    channel: MessageChannelModel,
    val embed: Array<String>
) :
    GenericGatewayMessageEvent(messageId, channel) {
    companion object {
        fun fromMessageEmbedEvent(event: MessageEmbedEvent): GatewayMessageEmbedEvent {
            return GatewayMessageEmbedEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                event.messageEmbeds.map { it.toJSONObject().toString(2) }.toTypedArray()
            )
        }
    }
}

class GatewayMessageReceivedEvent(
    messageId: Long,
    channel: MessageChannelModel,
    val message: MessageModel
    ) :
    GenericGatewayMessageEvent(messageId, channel) {
    companion object {
        fun fromMessageReceivedEvent(event: MessageReceivedEvent): GatewayMessageReceivedEvent {
            return GatewayMessageReceivedEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}

class GatewayMessageUpdateEvent(
    messageId: Long,
    channel: MessageChannelModel,
    val message: MessageModel
    ) :
    GenericGatewayMessageEvent(messageId, channel) {
    companion object {
        fun fromMessageUpdateEvent(event: MessageUpdateEvent): GatewayMessageUpdateEvent {
            return GatewayMessageUpdateEvent(
                event.messageIdLong,
                MessageChannelModel.fromMessageChannel(event.channel),
                MessageModel.fromMessage(event.message)
            )
        }
    }
}