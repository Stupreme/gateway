package org.discordlist.gateway.discord

import net.dv8tion.jda.bot.sharding.ShardManager

interface IDiscord {

    fun getShardManager(): ShardManager
}