package org.discordlist.gateway.core

import com.google.gson.GsonBuilder
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.hooks.SubscribeEvent
import org.apache.logging.log4j.LogManager
import org.discordlist.gateway.discord.models.GuildModel
import org.discordlist.gateway.discord.models.InviteModel
import org.discordlist.gateway.discord.models.UserModel
import org.discordlist.gateway.discord.models.WebhookModel
import org.discordlist.gateway.io.rabbitmq.RabbitMQSource

class EventForwarder(private val rabbitMQ: RabbitMQSource) {

    private val log = LogManager.getLogger(EventForwarder::class.java)
    private val gson = GsonBuilder().create()

    @SubscribeEvent
    fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        log.debug("[EventForwarder] MessageReceivedEvent got called.")
        log.debug(gson.toJson(UserModel.fromUser(event.author)))
        log.debug(gson.toJson(GuildModel.fromGuild(event.guild)))
        log.debug(gson.toJson(WebhookModel.fromWebhook(event.guild.webhooks.complete()[0])))
        log.debug(gson.toJson(InviteModel.fromInvite(event.guild.invites.complete()[0])))
        //rabbitMQ.channel.basicPublish(RabbitMQ.Exchanges.DISCORD_DISPATCH.key, "guild_message_received", null, serialized.toByteArray())
    }
}