package org.discordlist.gateway.core

import net.dv8tion.jda.core.hooks.AnnotatedEventManager
import net.dv8tion.jda.core.hooks.IEventManager
import org.discordlist.gateway.core.packets.TestPacket
import org.discordlist.gateway.discord.Discord
import org.discordlist.gateway.discord.IDiscord
import org.discordlist.gateway.io.ConfigLoader
import org.discordlist.gateway.io.RedisSource
import org.discordlist.gateway.io.rabbitmq.RabbitMQ
import org.discordlist.gateway.io.rabbitmq.RabbitMQSource
import org.simpleyaml.configuration.file.YamlFile

import java.io.IOException

class Gateway @Throws(IOException::class)
constructor() : IGateway {

    override val config: YamlFile
    override val rabbitMQSource: RabbitMQSource
    override val redisSource: RedisSource
    override val eventManager: IEventManager
    override val discord: IDiscord
    override val dev: Boolean
    private val systemGatewayHandler: SystemGatewayHandler

    init {
        RabbitMQUtil.setGateway(this)
        eventManager = AnnotatedEventManager()

        config = ConfigLoader("gateway.yml").load()

        dev = config.getBoolean("dev")

        rabbitMQSource = RabbitMQSource(
            this,
            config.getString("rabbitmq.host"),
            config.getString("rabbitmq.username"),
            config.getString("rabbitmq.password")
        )
        initRabbitMQ()

        redisSource = RedisSource(
            this,
            config.getString("redis.host"),
            config.getString("redis.password")
        )

        systemGatewayHandler = SystemGatewayHandler(this)
        systemGatewayHandler.send(TestPacket())
        discord = Discord(this, config)
    }

    @Throws(IOException::class)
    private fun initRabbitMQ() {

        val channel = rabbitMQSource.channel
        channel.exchangeDeclare(RabbitMQUtil.getExchange(RabbitMQ.Exchanges.SYSTEM_GATEWAY), "fanout")
        channel.exchangeDeclare(RabbitMQUtil.getExchange(RabbitMQ.Exchanges.LOGS), "fanout")
        channel.queueDeclare(RabbitMQUtil.getQueue(RabbitMQ.Queues.DISCORD_DISPATCH), true, false, false, null)
    }

    companion object {

        val API_VERSION = "v1"
    }
}
