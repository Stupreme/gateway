package org.discordlist.gateway.core.packets

interface SendablePacket {

    fun build(): String
}