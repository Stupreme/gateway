package org.discordlist.gateway.core.packets

import org.discordlist.gateway.io.rabbitmq.RabbitMQ
import org.json.JSONObject

abstract class OpPacket(val op: RabbitMQ.Op) : SendablePacket {

    protected var data: JSONObject = JSONObject()

    override fun build(): String {
        return JSONObject().put("op", op.value).put("data", data).toString()
    }
}