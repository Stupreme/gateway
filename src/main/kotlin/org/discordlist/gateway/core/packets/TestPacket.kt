package org.discordlist.gateway.core.packets

import org.discordlist.gateway.io.rabbitmq.RabbitMQ

class TestPacket : OpPacket(RabbitMQ.Op.TEST)