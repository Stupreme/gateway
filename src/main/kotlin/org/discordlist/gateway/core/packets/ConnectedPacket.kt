package org.discordlist.gateway.core.packets

import org.discordlist.gateway.io.rabbitmq.RabbitMQ

class ConnectedPacket() : OpPacket(RabbitMQ.Op.CONNECTED) {
}