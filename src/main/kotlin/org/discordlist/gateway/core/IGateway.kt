package org.discordlist.gateway.core

import net.dv8tion.jda.core.hooks.IEventManager
import org.discordlist.gateway.discord.IDiscord
import org.discordlist.gateway.io.RedisSource
import org.discordlist.gateway.io.rabbitmq.RabbitMQSource
import org.simpleyaml.configuration.file.YamlFile

interface IGateway {

    val config: YamlFile

    val rabbitMQSource: RabbitMQSource

    val redisSource: RedisSource

    val eventManager: IEventManager

    val discord: IDiscord

    val dev: Boolean
}
