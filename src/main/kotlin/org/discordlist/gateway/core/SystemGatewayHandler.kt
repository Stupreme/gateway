package org.discordlist.gateway.core

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import org.apache.logging.log4j.LogManager
import org.discordlist.gateway.core.packets.SendablePacket
import org.discordlist.gateway.io.rabbitmq.RabbitMQ
import java.nio.charset.StandardCharsets

class SystemGatewayHandler(private val gateway: IGateway) : DefaultConsumer(gateway.rabbitMQSource.channel) {

    private val log = LogManager.getLogger(SystemGatewayHandler::class.java)
    private val queueName = channel.queueDeclare().queue

    init {
        channel.queueBind(queueName, RabbitMQUtil.getExchange(RabbitMQ.Exchanges.SYSTEM_GATEWAY), "")
        channel.basicConsume(queueName, true, this)
    }

    override fun handleDelivery(
        consumerTag: String,
        envelope: Envelope,
        properties: AMQP.BasicProperties,
        body: ByteArray
    ) {
        val message = String(body, StandardCharsets.UTF_8)
        log.debug("[SystemGatewayHandler] Received message: $message")
    }

    fun send(packet: SendablePacket) {
        channel.basicPublish(RabbitMQUtil.getExchange(RabbitMQ.Exchanges.SYSTEM_GATEWAY), "", null, packet.build().toByteArray())
        log.debug("[SystemGatewayHandler] Sent message: ${packet.build()}")
    }
}
