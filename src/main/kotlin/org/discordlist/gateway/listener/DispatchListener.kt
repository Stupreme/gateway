package org.discordlist.gateway.listener

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import java.nio.charset.StandardCharsets

class DispatchListener(channel: Channel) : DefaultConsumer(channel) {

    override fun handleDelivery(
        consumerTag: String,
        envelope: Envelope,
        properties: AMQP.BasicProperties,
        body: ByteArray
    ) {
        val msg = String(body, StandardCharsets.UTF_8)
        println(msg)
        channel.basicAck(envelope.deliveryTag, false)
    }
}