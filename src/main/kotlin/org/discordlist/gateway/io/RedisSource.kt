package org.discordlist.gateway.io

import org.apache.logging.log4j.LogManager
import org.discordlist.gateway.core.IGateway
import org.discordlist.gateway.events.RedisConnectedEvent
import redis.clients.jedis.Jedis

class RedisSource(gateway: IGateway, host: String, password: String) {

    private val log = LogManager.getLogger(RedisSource::class.java)
    val connection: Jedis = Jedis(host)

    init {
        connection.auth(password)
        gateway.eventManager.handle(RedisConnectedEvent(gateway, this))
        log.info("[Redis] Connected to: {}", host)
    }
}