package org.discordlist.gateway.io

import org.simpleyaml.configuration.file.YamlFile

class ConfigLoader(private val file: String) {

    fun load(): YamlFile {
        val config = YamlFile(file)
        config.createNewFile(false)
        config.load()
        setDefaults(config)
        config.save()
        return config
    }

    private fun setDefaults(config: YamlFile) {
        // General
        saveDefault(config, "dev", false)
        // Bot
        saveDefault(config, "discord.token", "discordBotToken")

        // RabbitMQ
        saveDefault(config, "rabbitmq.host", "example.com")
        saveDefault(config, "rabbitmq.username", "user")
        saveDefault(config, "rabbitmq.password", "password")

        //Redis
        saveDefault(config, "redis.host", "example.com")
        saveDefault(config, "redis.password", "password")
    }

    private fun saveDefault(config: YamlFile, path: String, value: Any) {
        if (!config.isSet(path))
            config.set(path, value)
    }
}