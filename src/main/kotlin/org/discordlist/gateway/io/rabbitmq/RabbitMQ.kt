package org.discordlist.gateway.io.rabbitmq

class RabbitMQ {

    enum class Queues(val key: String) {
        DISCORD_DISPATCH("dlo.discord.dispatch")
    }

    enum class Exchanges(val key: String) {
        SYSTEM_GATEWAY("dlo.system.gateway"),
        LOGS("dlo.logs"),
    }

    enum class Op(val value: Int) {
        TEST(1337),
        CONNECTED(0)
    }
}