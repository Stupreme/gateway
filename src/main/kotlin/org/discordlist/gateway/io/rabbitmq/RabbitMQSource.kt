package org.discordlist.gateway.io.rabbitmq

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import org.apache.logging.log4j.LogManager
import org.discordlist.gateway.core.IGateway
import org.discordlist.gateway.events.RabbitMQConnectedEvent

class RabbitMQSource(gateway: IGateway, host: String, username: String, password: String) {

    private val log = LogManager.getLogger(RabbitMQSource::class.java)
    val connection: Connection
    val channel: Channel

    init {
        val factory = ConnectionFactory()
        factory.host = host
        factory.username = username
        factory.password = password
        connection = factory.newConnection()
        channel = connection.createChannel()
        log.info("[RabbitMQ] Connected to: {}", host)
        gateway.eventManager.handle(RabbitMQConnectedEvent(gateway, this))
    }
}