package org.discordlist.gateway.events

import org.discordlist.gateway.core.IGateway
import org.discordlist.gateway.io.rabbitmq.RabbitMQSource

class RabbitMQConnectedEvent(gateway: IGateway, val rabbitMQSource: RabbitMQSource) : GatewayEvent(gateway)