package org.discordlist.gateway.events

import net.dv8tion.jda.core.events.Event
import org.discordlist.gateway.core.EmptyJDA
import org.discordlist.gateway.core.IGateway

abstract class GatewayEvent(val gateway: IGateway) : Event(EmptyJDA.JDA)