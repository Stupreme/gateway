package org.discordlist.gateway.events

import org.discordlist.gateway.core.IGateway
import org.discordlist.gateway.io.RedisSource

class RedisConnectedEvent(gateway: IGateway, val redis: RedisSource): GatewayEvent(gateway)